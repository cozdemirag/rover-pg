﻿using Moq;
using NUnit.Framework;
using RoverPGDemo.Logic.Interfaces;
using RoverPGDemo.Logic.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace RoverPGDemo.Logic.UnitTests
{
    public class PlutoRoverSensorTests
    {
        private ISensor sensor;
        private Mock<IPlanet> mockPlanet;

        [SetUp]
        public void SetUp()
        {
            this.mockPlanet = new Mock<IPlanet>();
            this.mockPlanet.Setup(it => it.Obstacles).Returns(new List<(int, int)> { });
            this.sensor = new PlutoRoverSensor(this.mockPlanet.Object);
        }

        [Test]
        public void IsPositionEmpty_OutOfBounds_NoObstacles_SensorConfirmsMove_And_ReturnsCorrectCoordinates()
        {
            this.mockPlanet.Setup(it => it.MaxX).Returns(5);
            this.mockPlanet.Setup(it => it.MaxY).Returns(5);

            var actual = this.sensor.IsPositionEmpty(5, 0);

            Assert.IsTrue(actual);
        }

        [Test]
        public void IsPositionEmpty_WithInPlanetBoundaries_NoObstacles_SensorConfirmsMove_And_Coordinates()
        {
            this.mockPlanet.Setup(it => it.MaxX).Returns(5);
            this.mockPlanet.Setup(it => it.MaxY).Returns(5);

            var actual = this.sensor.IsPositionEmpty(3, 2);

            Assert.IsTrue(actual);
        }


        [Test]
        public void IsPositionEmpty_WithInPlanetBoundaries_ObstacleAhead_SensorDeclinesMove()
        {
            this.mockPlanet.Setup(it => it.MaxX).Returns(5);
            this.mockPlanet.Setup(it => it.MaxY).Returns(5);
            this.mockPlanet.Setup(it => it.Obstacles).Returns(new List<(int, int)>
            {
                (3, 2),
                (4, 1)
            });

            var actual = this.sensor.IsPositionEmpty(3, 2);

            Assert.IsFalse(actual);
        }

        [TestCase(5, 2)]
        [TestCase(2, 5)]
        public void IsPositionEmpty_OutOfPlanetBoundaries_ObstacleAhead_SensorDeclinesMove(
            int nextMoveX, int nextMoveY)
        {
            this.mockPlanet.Setup(it => it.MaxX).Returns(5);
            this.mockPlanet.Setup(it => it.MaxY).Returns(5);
            this.mockPlanet.Setup(it => it.Obstacles).Returns(new List<(int, int)>
            {
                (2, 0),
                (0, 2)
            });

            var actual = this.sensor.IsPositionEmpty(nextMoveX, nextMoveY);

            Assert.IsFalse(actual);
        }
    }
}
