using NUnit.Framework;
using RoverPGDemo.Logic.Interfaces;
using RoverPGDemo.Logic.Services;

namespace Tests
{
    public class PlutoRoverCommandValidatorTests
    {
        private ICommandValidator commandValidator;

        [SetUp]
        public void Setup()
        {
            commandValidator = new PlutoRoverCommandValidator();
        }

        [TestCase("F")]
        [TestCase("B")]
        [TestCase("L")]
        [TestCase("R")]
        public void Validate_Single_Valid_Command_ReturnsTrue(string givenCommand)
        {

            var actual = commandValidator.Validate(givenCommand);

            Assert.IsTrue(actual);
        }

        [TestCase("FFBBLRLR")]
        [TestCase("FFFFF")]
        [TestCase("FFRFF")]
        public void Validate_Multiple_Valid_Commands_ReturnsTrue(string givenCommand)
        {
            var actual = this.commandValidator.Validate(givenCommand);

            Assert.IsTrue(actual);
        }

        [TestCase("C")]
        [TestCase("D")]
        [TestCase("")]
        [TestCase("   ")]
        [TestCase(null)]
        [TestCase("1")]
        public void Validate_Single_Invalid_Command_ReturnsFalse(string givenCommand)
        {
            var actual = this.commandValidator.Validate(givenCommand);

            Assert.False(actual);
        }

        [TestCase("FFBR.")]
        [TestCase("CFFB")]
        public void Validate_Multiple_Invalid_Command_ReturnsFalse(string givenCommand)
        {
            var actual = this.commandValidator.Validate(givenCommand);

            Assert.False(actual);
        }
    }
}