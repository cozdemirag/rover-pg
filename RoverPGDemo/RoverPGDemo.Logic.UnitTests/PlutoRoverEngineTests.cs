﻿using Moq;
using NUnit.Framework;
using RoverPGDemo.Logic.Interfaces;
using RoverPGDemo.Logic.Models;
using RoverPGDemo.Logic.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace RoverPGDemo.Logic.UnitTests
{
    public class PlutoRoverEngineTests
    {
        private Mock<ISensor> mockSensor;

        [SetUp]
        public void SetUp()
        {
            this.mockSensor = new Mock<ISensor>();
            this.mockSensor.Setup(it => it.IsPositionEmpty(It.IsAny<int>(), It.IsAny<int>())).Returns(true);
        }


        [Test]
        public void Move_Heading_North_Move_Forward_Incrase_Y_Coordinate()
        {
            // Arrange
            var startingX = 0;
            var startingY = 0;
            var direction = "N";
            var sut = new PlutoRoverEngine(this.mockSensor.Object, startingX, startingY, direction);

            // Act
            var actual = sut.Move("F");

            // Assert
            Assert.AreEqual(0, actual.X);
            Assert.AreEqual(1, actual.Y);
            Assert.AreEqual("N", actual.Direction);
        }

        [Test]
        public void Move_Heading_North_Move_Backwards_Decreases_Y_Coordinate()
        {
            // Arrange
            var startingX = 0;
            var startingY = 10;
            var direction = "N";
            var sut = new PlutoRoverEngine(this.mockSensor.Object, startingX, startingY, direction);

            // Act
            var actual = sut.Move("B");

            // Assert
            Assert.AreEqual(0, actual.X);
            Assert.AreEqual(9, actual.Y);
            Assert.AreEqual("N", actual.Direction);
        }

        [Test]
        public void Rotate_Facing_North_Turn_Left_Faces_West()
        {
            // Arrange
            var sut = new PlutoRoverEngine(this.mockSensor.Object, 0, 0, "N");

            // ACt
            var actual = sut.Rotate("L");

            // Assert
            Assert.AreEqual("W", actual.Direction);
            Assert.AreEqual(0, sut.X);
            Assert.AreEqual(0, sut.Y);
        }

        [Test]
        public void Rotate_Facing_North_Turn_Right_Faces_East()
        {
            var sut = new PlutoRoverEngine(this.mockSensor.Object, 13, 88, "N");

            var actual = sut.Rotate("R");

            Assert.AreEqual("E", actual.Direction);
            Assert.AreEqual(13, sut.X);
            Assert.AreEqual(88, sut.Y);
        }

        [Test]
        public void Rotate_Facing_South_Turn_Left_4_Times_Faces_South()
        {
            // Arrange
            var sut = new PlutoRoverEngine(this.mockSensor.Object, 21, 5, "S");

            // Act
            sut.Rotate("L");
            sut.Rotate("L");
            sut.Rotate("L");
            var actual = sut.Rotate("L");

            // Assert
            Assert.AreEqual("S", actual.Direction);
            Assert.AreEqual(21, sut.X);
            Assert.AreEqual(5, sut.Y);
        }

        [Test]
        public void Rotate_Facing_East_Turn_Right_4_Times_Faces_East()
        {
            // Arrange
            var sut = new PlutoRoverEngine(this.mockSensor.Object, 23, 6, "E");

            // Act
            sut.Rotate("R");
            sut.Rotate("R");
            sut.Rotate("R");
            var actual = sut.Rotate("R");

            // Assert
            Assert.AreEqual("E", actual.Direction);
            Assert.AreEqual(23, sut.X);
            Assert.AreEqual(6, sut.Y);
        }
    }
}
