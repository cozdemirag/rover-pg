﻿namespace RoverPGDemo.Logic.Interfaces
{
    public interface ICommandValidator
    {
        bool Validate(string input);
    }
}
