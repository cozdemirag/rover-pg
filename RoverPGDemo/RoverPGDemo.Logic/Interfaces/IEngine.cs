﻿using RoverPGDemo.Logic.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace RoverPGDemo.Logic.Interfaces
{
    public interface IEngine
    {
        MovementResult Move(string input);

        RotationResult Rotate(string input);
    }
}
