﻿using RoverPGDemo.Logic.Models;

namespace RoverPGDemo.Logic.Interfaces
{
    public interface ISensor
    {
        bool IsPositionEmpty(int x, int y);
    }
}
