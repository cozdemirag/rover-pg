﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RoverPGDemo.Logic.Interfaces
{
    public interface IVehicle
    {
        void Move(string input);
    }
}
