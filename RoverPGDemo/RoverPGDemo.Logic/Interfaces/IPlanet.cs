﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RoverPGDemo.Logic.Interfaces
{
    public interface IPlanet
    {
        int MaxX { get; }
        int MaxY { get; }

        List<(int x, int y)> Obstacles { get; }
    }
}
