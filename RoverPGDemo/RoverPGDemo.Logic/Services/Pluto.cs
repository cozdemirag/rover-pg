﻿using RoverPGDemo.Logic.Interfaces;
using System.Collections.Generic;

namespace RoverPGDemo.Logic.Services
{
    public class Pluto : IPlanet
    {
        public int MaxX { get; }
        public int MaxY { get; }
        public List<(int x, int y)> Obstacles { get; }

        public Pluto()
            // for demo purposes
            : this(100, 100, new List<(int x, int y)> { (3, 3) })
        {
        }

        public Pluto(int maxX, int maxY, List<(int, int)> obstacles)
        {
            this.MaxX = maxX;
            this.MaxY = maxY;
            this.Obstacles = obstacles;
        }
    }
}
