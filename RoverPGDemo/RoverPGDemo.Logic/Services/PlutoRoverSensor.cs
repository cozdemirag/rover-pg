﻿using RoverPGDemo.Logic.Interfaces;
using RoverPGDemo.Logic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RoverPGDemo.Logic.Services
{
    public class PlutoRoverSensor : ISensor
    {
        private readonly IPlanet planet;

        public PlutoRoverSensor(IPlanet planet)
        {
            this.planet = planet;
        }

        public bool IsPositionEmpty(int rawX, int rawY)
        {
            var x = rawX >= planet.MaxX
                ? rawX - planet.MaxX
                : rawX < 0 ? planet.MaxX - rawX : rawX;
            var y = rawY >= planet.MaxY 
                ? rawY - planet.MaxY 
                : rawY < 0 ? planet.MaxY - rawY : rawY;

            return !planet.Obstacles.Any(it => it.x == x && it.y == y);
        }
    }
}
