﻿using RoverPGDemo.Logic.Interfaces;
using RoverPGDemo.Logic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RoverPGDemo.Logic.Services
{
    public class PlutoRoverEngine : IEngine
    {
        private static string[] DIRECTIONS = new string[4] { "W", "N", "E", "S" };

        private readonly ISensor sensor;

        public int X { get; private set; }
        public int Y { get; private set; }
        public string Direction { get; private set; }

        private int directionIndex = 0;
        private int XMultiplier;
        private int YMultiplier;

        public PlutoRoverEngine(ISensor sensor, int x, int y, string direction)
        {
            this.sensor = sensor;
            this.X = x;
            this.Y = y;
            this.Direction = direction;
            this.Initialize(direction);
        }

        public PlutoRoverEngine(ISensor sensor) : this(sensor, 0, 0, "N") { }

        public MovementResult Move(string input)
        {
            var directionMuptiplier = 0;

            if(input == "F")
            {
                directionMuptiplier = 1;
            }
            else if(input == "B")
            {
                directionMuptiplier = -1;
            }
            else
            {
                throw new ArgumentException($"{input} is not valid a command for movement");
            }

            var xToCheck = this.X + this.XMultiplier * directionMuptiplier;
            var yToCheck = this.Y + this.YMultiplier * directionMuptiplier;

            if (this.sensor.IsPositionEmpty(xToCheck, yToCheck))
            {
                this.X = xToCheck;
                this.Y = yToCheck;
            }

            return new MovementResult
            {
                Direction = this.Direction,
                X = this.X,
                Y = this.Y
            };
        }

        public RotationResult Rotate(string input)
        {
            var indexDirection = -1;

            if(input == "L")
            {
                indexDirection = -1;
            }
            else if (input == "R")
            {
                indexDirection = 1;
            }
            else
            {
                throw new ArgumentException($"Invalid rotation command {input} received.");
            }

            var rotation = directionIndex + indexDirection;
            if(rotation < 0)
            {
                directionIndex = DIRECTIONS.Length + rotation;
            }
            else if(rotation >= DIRECTIONS.Length)
            {
                directionIndex = rotation - DIRECTIONS.Length;
            }
            else
            {
                directionIndex = rotation;
            }

            this.Direction = DIRECTIONS[directionIndex];

            return new RotationResult
            {
                Direction = Direction
            };
        }

        private void Initialize(string direction)
        {
            this.directionIndex = GetDirectionIndex(direction);
            SetAxisMultipliers(direction);
        }

        private int GetDirectionIndex(string direction)
        {
            for (var index = 0; index < DIRECTIONS.Length; index++)
            {
                if (direction == DIRECTIONS[index])
                {
                    return index;
                }
            }

            throw new ArgumentException($"Invalid direction {direction}");
        }

        private void SetAxisMultipliers(string direction)
        {
            this.XMultiplier = direction == "W" || direction == "E" ? 1 : 0;
            this.YMultiplier = direction == "N" || direction == "S" ? 1 : 0;
        }
    }
}
