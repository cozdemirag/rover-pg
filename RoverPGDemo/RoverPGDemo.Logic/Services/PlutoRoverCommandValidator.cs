﻿using RoverPGDemo.Logic.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace RoverPGDemo.Logic.Services
{
    public class PlutoRoverCommandValidator : ICommandValidator
    {
        private static List<string> VALID_COMMANDS = new List<string>(4) { "F", "B", "L", "R" };

        public bool Validate(string input)
        {
            if (string.IsNullOrWhiteSpace(input))
            {
                return false;
            }

            for(var index = 0; index < input.Length; index++)
            {
                var command = input[index].ToString();

                if (string.IsNullOrWhiteSpace(command))
                {
                    return false;
                }

                if (!VALID_COMMANDS.Contains(command))
                {
                    return false;
                }
            }

            return true;
        }
    }
}
