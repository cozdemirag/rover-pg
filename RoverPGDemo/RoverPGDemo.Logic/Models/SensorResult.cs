﻿namespace RoverPGDemo.Logic.Models
{
    public class SensorResult
    {
        public bool CanMove { get; }
        public int X { get; }
        public int Y { get; }

        public SensorResult(bool canMove, int posX, int posY)
        {
            this.CanMove = canMove;
            this.X = posX;
            this.Y = posY;
        }
    }
}
