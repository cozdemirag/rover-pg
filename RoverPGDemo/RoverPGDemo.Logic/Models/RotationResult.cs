﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RoverPGDemo.Logic.Models
{
    public class RotationResult
    {
        public int HorizontalMultiplier { get; set; }

        public int VerticalMultiplier { get; set; }

        public string Direction { get; set; }
    }
}
