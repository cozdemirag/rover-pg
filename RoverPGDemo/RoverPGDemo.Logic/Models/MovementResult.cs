﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RoverPGDemo.Logic.Models
{
    public class MovementResult
    {
        public int X;
        public int Y;
        public string Direction;
    }
}
